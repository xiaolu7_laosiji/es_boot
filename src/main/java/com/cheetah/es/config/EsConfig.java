package com.cheetah.es.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.stream.Collectors;

@Configuration
public class EsConfig {
    @Value("${elasticsearch.host}")
    private String esHost;

    @Bean
    RestHighLevelClient restEsClient() {
        String[] arr = esHost.split(",");
        HttpHost[] hosts = Arrays.stream(arr).map(item -> {
            String[] ipHost = item.split(":");
            HttpHost host = new HttpHost(ipHost[0],
                    Integer.parseInt(ipHost[1]));
            return host;
        }).toArray(HttpHost[]::new);
        return new RestHighLevelClient(RestClient.builder(hosts));
    }
}
