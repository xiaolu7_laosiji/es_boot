package com.cheetah.es.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cheetah.es.entity.Position;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PositionMapper extends BaseMapper<Position> {
}
